<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    public function person()
	{
		return $this->hasOne(Person::class, 'id', 'person_id');
	}
}
