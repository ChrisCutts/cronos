<?php

use Illuminate\Database\Seeder;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(App\Person::class, 50)->create()->each(function ($person) {
			$rand = random_int(0,50);
			for($l=1;$l<$rand;$l++) {
				$person->times()->save(factory(App\Time::class)->make());
			}
		});
    }
}
